WIP

# Books

- [Cracking the Code
  interview](https://www.amazon.com/Cracking-Coding-Interview-Programming-Questions/dp/0984782850/ref=sr_1_1?crid=17W0MLBPSGC7&dchild=1&keywords=cracking+the+coding+interview&qid=1595963983&sprefix=cracking+the+cofi%2Caps%2C321&sr=8-1)
- The C Programming Language
- The C++ Programming Language
- Sams Teach Yourself C++ in One Hour a Day

# Websites

- [HackerRank](hackerrank.com)
- [Interview Cake](interviewcake.com) (paid)

# Videos

- [Irfan Baqui](https://www.youtube.com/channel/UCYvQTh9aUgPZmVH0wNHFa1A) Google
  employee
- [CS Dojo](https://www.youtube.com/c/CSDojo)
